package com.yingdongshuju.es;

import com.alibaba.fastjson.JSON;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.core.GetSourceRequest;
import org.elasticsearch.client.core.GetSourceResponse;
import org.elasticsearch.cluster.metadata.MetadataIndexTemplateService;
import org.elasticsearch.common.xcontent.XContent;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
class EsApplicationTests {
    @Autowired
    RestHighLevelClient restHighLevelClient;

    //根据id查询
    @Test
    void contextLoads() {
        GetSourceRequest getSourceRequest = new GetSourceRequest("user_db", "b-q0Y3kBJJfAIo_YKexY");

        try {
            GetSourceResponse documentFields = restHighLevelClient.getSource(getSourceRequest, RequestOptions.DEFAULT);
            System.out.println(documentFields.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //复合查询
    @Test
    void context() throws Exception {
        SearchRequest searchRequest = new SearchRequest("user_db");
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("id", "abc");
        searchSourceBuilder.query(termQueryBuilder);
        searchRequest.source(searchSourceBuilder);
        SearchResponse search = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        System.out.println(search.toString());
    }

    //是否存在
    public boolean exists(String index, String id) throws Exception {
        GetRequest getRequest = new GetRequest(index, id);
        boolean exists = restHighLevelClient.exists(getRequest, RequestOptions.DEFAULT);
        System.out.println(exists);
        return exists;
    }

    //修改
    @Test
    public void test01()throws Exception{
        String id="b-q0Y3kBJJfAIo_YKexY";
        if (exists("user_db",id)) {
            UpdateRequest updateRequest = new UpdateRequest("user_db",id);
            updateRequest.doc(createJson(new User("1","周大傻",20)),XContentType.JSON);
            UpdateResponse update = restHighLevelClient.update(updateRequest, RequestOptions.DEFAULT);
            System.out.println(update.toString());
        }

    }


    //根据id删除文档
    @Test
    public void test03() throws Exception {
        DeleteRequest user_db = new DeleteRequest("user_db");
        user_db.id("b-q0Y3kBJJfAIo_YKexY");
        DeleteResponse delete = restHighLevelClient.delete(user_db, RequestOptions.DEFAULT);
        System.out.println(delete.toString());
    }


    //批量操作
    @Test
    public void test02() throws Exception {
        BulkRequest bulkRequest = new BulkRequest("user_db");
        IndexRequest indexRequest = new IndexRequest();
        User user = new User("adki", "李四2", 18);
        indexRequest.id(user.getId());
        indexRequest.source(createMap(user));
        bulkRequest.add(indexRequest);
        UpdateRequest updateRequest = new UpdateRequest();
        updateRequest.id("b-q0Y3kBJJfAIo_YKexY");
        updateRequest.doc(createMap(new User("kkkk", "万物", 11)));
        bulkRequest.add(updateRequest);
        BulkResponse bulk = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
        System.out.println(bulk.toString());

    }


    private <T> Map createMap(T t) {
        String string = JSON.toJSONString(t);
        Map map = JSON.parseObject(string, Map.class);
        return map;
    }

    private <T> String createJson(T t) {
        String string = JSON.toJSONString(t);
        return string;
    }

}
